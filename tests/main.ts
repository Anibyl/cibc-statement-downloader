import { Download, expect, test } from '@playwright/test';

test('download statements', async ({ page }) => {
  // Longer test timeout; PDF downloading takes more time than the default 30s
  if (process.env.TIMEOUT) {
    test.setTimeout(parseInt(process.env.TIMEOUT));
  }

  await page.goto('https://www.cibconline.cibc.com/ebm-resources/online-banking/client/index.html#/auth/signon');

  await page.locator('//button[contains(@class,"onetrust-close-btn-handler onetrust-close-btn-ui banner-close-button ot-close-icon")]')?.click();

  await page.getByTestId("card-number-input").fill(process.env.USERNAME);
  await page.getByTestId("password-input").fill(process.env.PASSWORD);
  await page.getByTestId("primary-button").click();
  await page.getByTestId("otvc-send-btn").click();
  await page.getByTestId("otvc-verification-code-inputbox").focus();

  // Enter code manually!

  await page.waitForURL("https://www.cibconline.cibc.com/ebm-resources/online-banking/client/index.html#/accounts");

  // Assuming there's only one credit card
  await page.locator('//div[contains(@class,"credit-accounts")]//a[contains(@class,"account-name-header")]').click();
  await page.getByText("View eStatements").first().click();

  await page.waitForURL("https://www.cibconline.cibc.com/ebm-resources/public/banking/cibc/client/web/index.html#/accounts/online-statements/**/details");

  await page.waitForSelector('ui-collapsible-pane');
  const collapsedPaneXpath = '//ui-collapsible-pane[contains(@class,"ui-collapsed")]'
  do {
    await page.locator(collapsedPaneXpath).first().click()
  } while (await page.isVisible(collapsedPaneXpath))

  const downloadButtons = await page.getByLabel("Download this PDF").all();
  expect(downloadButtons.length).toBeGreaterThan(0);

  for (const downloadButton of downloadButtons) {
    const name: string = await downloadButton.innerText();
    const date = name.split(' to ')[1]
        .replace(/([A-Za-z]+) (\d{2}), (\d{4})/, "$3-$1-$2")
        .replace("January", "01")
        .replace("February", "02")
        .replace("March", "03")
        .replace("April", "04")
        .replace("May", "05")
        .replace("June", "06")
        .replace("July", "07")
        .replace("August", "08")
        .replace("September", "09")
        .replace("October", "10")
        .replace("November", "11")
        .replace("December", "12")

    const downloadPromise = page.waitForEvent('download');
    await downloadButton.focus();
    await downloadButton.click();
    const download: Download = await downloadPromise;
    await download.saveAs(`~/Downloads/cibc/${date} CIBC statement.pdf`);
  }
});
