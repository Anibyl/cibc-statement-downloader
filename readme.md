Playwright automation that downloads all credit card statements from CIBC

* create `.env` file using `.env.template`
* run the tests (e.g. via `npx playwright test`) in headed (`--headed`) mode
* enter code from push
* wait until the end
* check `~/Downloads/cibc` folder